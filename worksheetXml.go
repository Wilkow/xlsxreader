package xlsxreader

import (
	"encoding/xml"
	"io"
	"log"
	"strconv"
)

type Worksheet struct {
	Name string
	Rows []Row
}

type Row struct {
	Cells []Cell
}

type Cell struct {
	Type  string
	Value string
}

func readGridValuesFromWorksheetXml(xmlFile *io.ReadCloser, ws *Worksheet) {
	d := xml.NewDecoder(*xmlFile)
	token, err := d.RawToken()

	if err != nil {
		log.Fatal(err)
	}
SkippingLoop:
	for {
		switch se := token.(type) {
		case xml.StartElement:
			if se.Name.Local == "sheetData" {
				break SkippingLoop
			}
		}
		token, err = d.RawToken()
		if err != nil {
			log.Fatal("8", err)
		}
	}
	token, err = d.RawToken()
	if err != nil {
		log.Fatal("7", err)
	}
	var expectedRow int64 = 1
ReadingLoop:
	for {
		switch se := token.(type) {
		case xml.EndElement:
			if se.Name.Local == "sheetData" {
				break ReadingLoop
			}
		case xml.StartElement:
			if se.Name.Local == "row" {
				var row Row
				var expectedCol int = 1
				///
				token2, err := d.RawToken()
				if err != nil {
					log.Fatal("1", err)
				}
			LoopRow:
				for {

					switch se2 := token2.(type) {
					case xml.EndElement:
						if se2.Name.Local == se.Name.Local {
							break LoopRow
						}
					case xml.StartElement:

						if se2.Name.Local == "c" {
							var cell Cell
							////
							token3, err := d.RawToken()
							if err != nil {
								log.Fatal("2", err)
							}
						LoopCell:
							for {
								switch se3 := token3.(type) {
								case xml.EndElement:
									if se3.Name.Local == se2.Name.Local {
										break LoopCell
									}
								case xml.CharData:
									cell.Value = string(se3)
									// for _, attr := range se2.Attr {
									// 	if attr.Name.Local == "t" {
									// 		cell.Type = attr.Value
									// 		break
									// 	}
									// }
									cell.Type = se2.Attr[2].Value
									break LoopCell
								}
								token3, err = d.RawToken()
								if err != nil {
									log.Fatal("11", err)
								}
							}
							////
							var col_str string
							var col_num int
							col_str = getColFromRef(se2.Attr[0].Value)
							col_num = colDecode(col_str)

							if expectedCol != col_num {
								col_num := col_num
								diff := col_num - expectedCol
								row.Cells = append(row.Cells, make([]Cell, diff)...)
								expectedCol += diff
							}
							expectedCol += 1
							row.Cells = append(row.Cells, cell)
						}
						///

					}
					token2, err = d.RawToken()
					if err != nil {
						log.Fatal("3", err)
					}
				}
				///
				if err != nil {
					log.Fatal("4", err)
				}
				rowId, _ := strconv.ParseInt(se.Attr[0].Value, 10, 0)
				if rowId != expectedRow {
					diff := rowId - expectedRow
					ws.Rows = append(ws.Rows, make([]Row, diff)...)
					expectedRow += diff
				}
				ws.Rows = append(ws.Rows, row)
				expectedRow += 1
			}
		}
		token, err = d.RawToken()
		if err != nil {
			log.Fatal("5", err)
		}
	}
}
