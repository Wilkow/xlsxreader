package xlsxreader

import (
	"archive/zip"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"unicode"
)

// Returns raw data from XLSX file in form of slice containing each of excel sheets. Each Worksheet
// contains sheet name and slice of rows and cells for each row. Each row from spreadsheet xml file
// is filled with empty cells, if there are any empty in between cells in row or between entire rows.
func ReadXlsxFile(fileName string) []Worksheet {

	zipReader, err := zip.OpenReader(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer zipReader.Close()
	var wg sync.WaitGroup
	var setupWg sync.WaitGroup
	var ss []string
	var names []string
	results := make(chan Worksheet, len(zipReader.File))
	for _, file := range zipReader.File {
		switch file.Name {
		// Contains info about sheets and its names
		case "xl/workbook.xml":
			setupWg.Add(1)
			go readWorkbookXml(&setupWg, file, &names)
		// Contains shared strings from all sheets
		case "xl/sharedStrings.xml":
			setupWg.Add(1)
			go readSharedStringsXml(&setupWg, &ss, file)
		default:
			if strings.Contains(file.Name, "xl/worksheets/sheet") {
				id, err := strconv.ParseInt(file.Name[19:len(file.Name)-4], 10, 0)
				if err != nil {
					log.Fatal(err)
				}
				wg.Add(1)
				go readWorksheet(&wg, zipReader, results, id, &ss, &setupWg, &names)
			}
		}
	}

	// Wait for parsing of workbook/sharedstrings info xml's to finish
	// for other goroutines to use their results
	setupWg.Wait()
	// Wait for all goroutines to finish parsing sheets
	wg.Wait()

	var sheets []Worksheet
	for range len(names) {
		ws := <-results
		sheets = append(sheets, ws)
	}

	return sheets
}

func readWorksheet(wg *sync.WaitGroup, zipReader *zip.ReadCloser, res chan<- Worksheet, id int64, ss *[]string, wgS *sync.WaitGroup, names *[]string) {
	// Can't read file directly by name, need to iterate over names in zip
	for _, file := range zipReader.File {
		if file.Name == fmt.Sprintf("xl/worksheets/sheet%d.xml", id) {
			xmlFile, err := file.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer xmlFile.Close()
			var ws Worksheet
			readGridValuesFromWorksheetXml(&xmlFile, &ws)
			wgS.Wait()
			for i := range len(ws.Rows) {
				for j := range len(ws.Rows[i].Cells) {
					if ws.Rows[i].Cells[j].Type == "s" {
						ind, _ := strconv.ParseInt(ws.Rows[i].Cells[j].Value, 10, 0)
						ws.Rows[i].Cells[j].Value = (*ss)[ind]
					}
				}
			}
			ws.Name = (*names)[id-1]
			res <- ws
		}
	}
	defer wg.Done()
}

func readWorkbookXml(wg *sync.WaitGroup, file *zip.File, names *[]string) {
	xmlFile, err := file.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer xmlFile.Close()
	readSheetNamesFromWorkbookXml(&xmlFile, names)
	defer wg.Done()
}

func readSharedStringsXml(wg *sync.WaitGroup, ss *[]string, file *zip.File) {
	xmlFile, err := file.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer xmlFile.Close()
	readSharedStringsFromXml(&xmlFile, ss)
	defer wg.Done()
}

func getColFromRef(ref string) string {
	for i, char := range ref {
		if unicode.IsDigit(char) {
			return ref[:i]
		}
	}
	return ""
}
