package xlsxreader

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"testing"
)

// Very naive test but given all assumptions of reading are implemented
// in testFile.xlsx and checked it is fine as full parse test
// sometimes fails due to changes in parsing json
func TestComparison(t *testing.T) {
	sheetsExpected := new([]Worksheet)
	filename := "testFiles/testFileExpected.json"

	// Open the JSON file
	file, _ := os.Open(filename)
	defer file.Close()

	// Decode JSON from the file
	decoder := json.NewDecoder(file)
	decoder.Decode(&sheetsExpected)

	sheetsParsed := ReadXlsxFile("testFiles/testFile.xlsx")
	if len(sheetsParsed) != len(*sheetsExpected) {
		t.Errorf("Comparison test failed")
	}
	for _, sheetP := range sheetsParsed {
		for _, sheetE := range *sheetsExpected {
			if sheetP.Name == sheetE.Name {
				if !reflect.DeepEqual(sheetP, sheetE) {
					t.Errorf("Comparison test failed")
					fmt.Println("Expected:")
					fmt.Println(sheetsExpected)
					fmt.Println("Parsed:")
					fmt.Println(sheetsParsed)
				}
			}
		}
	}
}

func BenchmarkReadXlsxFile(b *testing.B) {
	filename := "testFiles/benchmarkFile.xlsx"
	for i := 0; i < b.N; i++ {
		ReadXlsxFile(filename)
	}
}

func TestReadWorkbook(t *testing.T) {
	filename := "testFiles/testFile.xlsx"
	zipReader, err := zip.OpenReader(filename)
	var xmlFile io.ReadCloser
	if err != nil {
		log.Fatal(err)
	}
	defer zipReader.Close()
	for _, file := range zipReader.File {
		if file.Name == "xl/workbook.xml" {
			xmlFile, err = file.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer xmlFile.Close()
		}
	}
	// Actual test
	var names []string
	readSheetNamesFromWorkbookXml(&xmlFile, &names)
	//fmt.Println(names)
	namesExpected := []string{"Sheet1", "Sheet2"}
	if !reflect.DeepEqual(names, namesExpected) {
		t.Errorf("Reading sheet names failed")
		fmt.Println("Expected:")
		fmt.Println(namesExpected)
		fmt.Println("Parsed:")
		fmt.Println(names)
	}
}

func TestReadSharedStrings(t *testing.T) {
	filename := "testFiles/testFile.xlsx"
	zipReader, err := zip.OpenReader(filename)
	var xmlFile io.ReadCloser
	if err != nil {
		log.Fatal(err)
	}
	defer zipReader.Close()
	for _, file := range zipReader.File {
		if file.Name == "xl/sharedStrings.xml" {
			xmlFile, err = file.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer xmlFile.Close()
		}
	}
	// Actual test
	var names []string
	readSharedStringsFromXml(&xmlFile, &names)
	//fmt.Println(names)
	namesExpected := []string{"htwse", "sger", "adwe", "egsr", "aef", "aefa", "asd", "xd"}
	if !reflect.DeepEqual(names, namesExpected) {
		t.Errorf("Reading sheet names failed")
		fmt.Println("Expected:")
		fmt.Println(namesExpected)
		fmt.Println("Parsed:")
		fmt.Println(names)
	}

}

func TestReadGridValuesFromWorksheetXml(t *testing.T) {
	filename := "testFiles/testFile.xlsx"
	zipReader, err := zip.OpenReader(filename)
	var xmlFile io.ReadCloser
	if err != nil {
		log.Fatal(err)
	}
	defer zipReader.Close()
	for _, file := range zipReader.File {
		if file.Name == "xl/worksheets/sheet2.xml" {
			xmlFile, err = file.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer xmlFile.Close()
		}
	}
	// Actual test
	var ws Worksheet
	readGridValuesFromWorksheetXml(&xmlFile, &ws)
	// for _, row := range ws.Rows {
	// 	fmt.Println(row)
	// }
	// namesExpected := []string{"htwse", "sger", "adwe", "egsr", "aef", "aefa", "asd", "xd"}
	// if !reflect.DeepEqual(names, namesExpected) {
	// 	t.Errorf("Reading sheet names failed")
	// 	fmt.Println("Expected:")
	// 	fmt.Println(namesExpected)
	// 	fmt.Println("Parsed:")
	// 	fmt.Println(names)
	// }

}
