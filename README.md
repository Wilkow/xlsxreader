# xlsxreader
Fast xlsx files reader written in pure Go
## Testing
```sh
go test -race . 
```
## Usage
```go
var fileXlsx string = "workbook.xlsx"
var wb []xlsxreader.Worksheet = xlsxreader.ReadXlsxFile(fileXlsx)
```