package xlsxreader

import (
	"encoding/xml"
	"io"
	"log"
)

// Reads string slice with sheet names from xl/workbook.xml file
func readSheetNamesFromWorkbookXml(xmlFile *io.ReadCloser, names *[]string) {
	d := xml.NewDecoder(*xmlFile)
	token, err := d.RawToken()
	if err != nil {
		log.Fatal(err)
	}
Loop:
	for {
		switch se := token.(type) {
		case xml.EndElement:
			if se.Name.Local == "workbook" {
				break Loop
			}
		case xml.StartElement:
			if se.Name.Local == "sheet" {
				*names = append(*names, se.Attr[0].Value)
			}
		}
		token, err = d.RawToken()
		if err != nil {
			log.Fatal(err)
		}
	}
}
