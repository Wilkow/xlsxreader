package xlsxreader

import (
	"encoding/xml"
	"io"
	"log"
	"strconv"
)

func readSharedStringsFromXml(xmlFile *io.ReadCloser, names *[]string) {
	d := xml.NewDecoder(*xmlFile)
	var token xml.Token
	var err error
	var uniqueStringsCount int64
	// Get to "uniqueCount" and create slice of appropriate size in advance
	for range 3 {
		token, err = d.RawToken()
		if err != nil {
			log.Fatal(err)
		}
	}
	switch s := token.(type) {
	case xml.StartElement:
		uniqueStringsCount, _ = strconv.ParseInt(s.Attr[2].Value, 10, 0)
	}
	*names = make([]string, uniqueStringsCount)
	stringId := 0
	// Read the rest of xml looking for text in "t" tags
Loop:
	for {
		switch se := token.(type) {
		case xml.EndElement:
			if se.Name.Local == "sst" {
				break Loop
			}
		case xml.StartElement:
			if se.Name.Local == "t" {
				token2, err := d.RawToken()
				if err != nil {
					log.Fatal(err)
				}
			Loop2:
				for {
					switch se2 := token2.(type) {
					case xml.EndElement:
						break Loop2
					case xml.CharData:
						(*names)[stringId] = string(se2)
						stringId++
					}
					token2, err = d.RawToken()
					if err != nil {
						log.Fatal(err)
					}
				}
			}
		}
		token, err = d.RawToken()
		if err != nil {
			log.Fatal(err)
		}
	}
}
