package xlsxreader

import (
	"testing"
)

func TestColDecoder(t *testing.T) {
	inputs := []string{"A", "Z", "AA", "AZ", "BA", "ZZ", "AAA"}
	expected := []int{1, 26, 27, 52, 53, 702, 703}

	for i := range len(inputs) {
		result := colDecode(inputs[i])
		if result != expected[i] {
			t.Errorf("Input %s", inputs[i])
			t.Errorf("Expected %d", expected[i])
			t.Errorf("Result %d", result)
		}
	}

}

func BenchmarkColDecoder(b *testing.B) {
	for i := 0; i < b.N; i++ {
		colDecode("CAAA")
	}
}
