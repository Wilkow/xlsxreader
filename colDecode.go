package xlsxreader

// Practically the same as below table but map access is slower
// var index = map[rune]int{
// 	'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6,
// 	'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12,
// 	'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17,
// 	'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22,
// 	'W': 23, 'X': 24, 'Y': 25, 'Z': 26,
// }

var index = [91]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26}

func colDecode(s string) int {
	ret := 0
	slen := len(s) - 1
	for i, r := range s {
		ret += index[r] * powInt(26, slen-i)
	}
	return ret
}

func powInt(x int, n int) int {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}
	y := powInt(x, n/2)
	if n%2 == 0 {
		return y * y
	}
	return x * y * y
}
